node {
    stage('Checkout'){
        checkout scm
        def packageJSON = readJSON file: 'package.json'
        if(packageJSON){
            echo "Building version ${packageJSON.version}"
        }
    }
    stage('Build'){
        withEnv(["PATH+jdk=${tool 'NodeJS'}/bin"]){
            sh "npm install"
            sh "npm run build-ci"
            archiveArtifacts artifacts: 'dist/**'
        }
    }
    stage('Tests') {
        withEnv(["PATH+jdk=${tool 'NodeJS'}/bin"]){
            if(!env.TEST_COVERAGE) {
                sh "npm run test-ci"
                archiveArtifacts artifacts: './coverage/lcov-report'
            } else {
                sh "npm run test-ci-without-coverage"
            }
        }
    }
    stage('Sonar'){
        def scannerHome = tool 'SonarScanner';
        withSonarQubeEnv('sonar-scanner') {
            sh "${scannerHome}/bin/sonar-scanner \
                -Dsonar.organization=${EXAM_SONAR_ORGANIZATION} \
                -Dsonar.projectKey=${EXAM_SONAR_PROJECTKEY} \
                -Dsonar.host.url=https://sonarcloud.io \
                -Dsonar.login=${EXAM_SONAR_LOGIN}"
        }
    }
}
